require 'httparty'

module Login
    include HTTParty
    base_uri 'https://api.github.com/users/'
    format :json
    headers Accept: '*/*',
            'Content-Type': 'application/json'
end