require 'json'

describe 'Busca de conta no github' do
    before do
        @repos = Login.get('/rpfiorin/repos')
    end

    context 'Casos de teste' do
        it 'Validar se status da requisicao retorna sucesso' do
            # assercao
            expect(@repos.code).to eq(200)
        end
        it 'Validar quantidade de repositórios do usuário' do
            parsed_json = JSON.parse(@repos.body)

            count_prj = 0
            parsed_json.each do |element|
                if (element["id"] != "")
                    count_prj = count_prj+1 
                end
            end

            puts parsed_json
            # assercao
            expect(count_prj).to eq 6   
        end
    end
end