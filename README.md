# Back-end test automation - Github

## How to use it

1. Clone this project with HTTPS option.
2. Install ruby 3.0.
3. In the terminal/cmd, execute the command: bundle install
4. After that, browse to the project folder and run: rspec
5. Finally, check the 'results' folder to open rspec_results file. 

Enjoy!